import random

class SquareMatrix(object):
    def __init__(self, size):
        self.data = [[random.randint(0, 10) for x in xrange(size)] for y in xrange(size)]

    def __repr__(self):
        return str(self.data)

    def __add__(self, other):
        pass

    def __sub__(self, other):
	new_data = [[0 for x in xrange(len(self.data))] for y in xrange(len(self.data))]
        if len(self.data) == len(other.data):
		for i in xrange(len(self.data)):
			for j in xrange(len(self.data)):
				new_data[i][j] = self.data[i][j] - other.data[i][j]
		print(new_data)
	else:
		print("Not equal sizes!")
			

    def __mul__(self, other):
        out = []
        size = len(self.data)
        assert size == len(other.data)

        for x in range(size):
            r = []
            for y in range(size):
                row1 = self.data[x]
                row2 = [other.data[i][y] for i in range(size)]
                r.append(sum([a * b for a, b in zip(row1, row2)]))
            out.append(r)

        r = self.__class__(size)
        r.data = out
        return r


print(SquareMatrix(10))

print(SquareMatrix(10) + SquareMatrix(10))
print(SquareMatrix(10) - SquareMatrix(10))
print(SquareMatrix(10) * SquareMatrix(10))